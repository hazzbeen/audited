module Audited
  VERSION = '3.0.0'

  class << self
    attr_accessor :ignored_attributes, :current_user_method, :cmd_uuid_method, :session_uuid_method, :audit_class
  end

  @ignored_attributes = %w(lock_version created_at updated_at created_on updated_on)

  @current_user_method = :current_user

  @cmd_uuid_method = :cmd_uuid
  @session_uuid_method = :session_uuid

end
